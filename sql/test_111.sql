/*
 Navicat Premium Data Transfer

 Source Server         : moli
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : test_111

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 07/12/2019 02:30:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for text
-- ----------------------------
DROP TABLE IF EXISTS `text`;
CREATE TABLE `text`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `message` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  `time` datetime(0) DEFAULT NULL COMMENT '发生时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of text
-- ----------------------------
INSERT INTO `text` VALUES (1, '1', '1', '2019-12-06 23:21:03');
INSERT INTO `text` VALUES (6, '2', '2', '2019-12-06 23:20:30');
INSERT INTO `text` VALUES (7, '3', '3', '2019-12-06 23:20:42');
INSERT INTO `text` VALUES (9, '6', '6', '2019-12-06 23:21:07');
INSERT INTO `text` VALUES (10, '8', '8', '2019-12-06 23:22:39');
INSERT INTO `text` VALUES (11, 'qer', 'rewrew', '2019-12-06 23:46:03');
INSERT INTO `text` VALUES (12, 'wrtt', 'reerter', '2019-12-06 23:47:21');
INSERT INTO `text` VALUES (13, '1', '1', '2019-12-06 23:48:05');
INSERT INTO `text` VALUES (14, '2', '2', '2019-12-06 23:48:19');
INSERT INTO `text` VALUES (15, 'errwer', 'rewrewr', '2019-12-07 00:10:00');
INSERT INTO `text` VALUES (16, 'wasdas', 'dasda', '2019-12-07 00:11:09');
INSERT INTO `text` VALUES (17, '3213', '1213', '2019-12-07 00:17:19');
INSERT INTO `text` VALUES (18, 'e\'q\'w', '恶趣味群', '2019-12-07 00:17:34');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL COMMENT 'id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '1', '1');

SET FOREIGN_KEY_CHECKS = 1;
