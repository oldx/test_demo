package com.test.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.demo.entity.Text;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description: text
 * @Author: moli
 * @Date:   2019-12-06
 * @Version: V1.0
 */
@Mapper
public interface TextMapper extends BaseMapper<Text> {

    /**
     * 模糊条件分页查询
     * @param dom
     * @param value
     * @param page
     * @param limit
     * @return
     */
    @Select("select * from text where ${dom} like '%' #{value} '%' limit #{page},#{limit}")
    public List<Text> findTextby(@Param("dom") String dom, @Param("value") String value, @Param("page") int page,  @Param("limit") int limit);
}
