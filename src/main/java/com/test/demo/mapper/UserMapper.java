package com.test.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: text
 * @Author: moli
 * @Date:   2019-12-06
 * @Version: V1.0
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
