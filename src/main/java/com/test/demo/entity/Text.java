package com.test.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description: text
 * @Author: moli
 * @Date:   2019-12-06
 * @Version: V1.0
 */
@Data
@TableName("text")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="text对象", description="text")
public class Text {
    
	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
	private Integer id;
	/**标题*/
    @ApiModelProperty(value = "标题")
	private String title;
	/**内容*/
    @ApiModelProperty(value = "内容")
	private String message;
	/**发生时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发生时间")
	private Date time;
}
