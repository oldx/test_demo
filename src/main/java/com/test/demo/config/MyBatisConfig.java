package com.test.demo.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author: moli
 * @Email: 974751082@qq.com
 * @qq: 974751082
 * @Date: 2019/12/6 21:03
 */
@Configuration
@EnableTransactionManagement
@MapperScan("com.test.demo.mapper.*")
public class MyBatisConfig {
}
