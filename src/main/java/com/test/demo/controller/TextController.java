package com.test.demo.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.test.demo.entity.Text;
import com.test.demo.mapper.TextMapper;
import com.test.demo.service.ITextService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: text
 * @Author: moli
 * @Date: 2019-12-06
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "text")
@RestController
@RequestMapping("text")
public class TextController extends JeecgController<Text, ITextService> {
    @Autowired
    private ITextService textService;
    @Autowired
    private TextMapper textMapper;

    /**
     * 分页列表查询
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "text-分页列表查询")
    @ApiOperation(value = "text-分页列表查询", notes = "text-分页列表查询")
    @GetMapping(value = "/list")
    public Map<String, Object> queryPageList(
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "limit", defaultValue = "10") Integer limit
    ) {
        Page<Text> page1 = new Page<>(page, limit);
        IPage<Text> pageList = textService.page(page1);
        List<Text> textList = pageList.getRecords();
        int count = textService.count();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg", "成功");
        map.put("code", 0);
        map.put("count", count);
        map.put("data", textList);
        return map;
    }

    @AutoLog(value = "text-模糊分页列表查询")
    @ApiOperation(value = "text-分页列表查询", notes = "text-分页列表查询")
    @PostMapping(value = "/listBy")
    public Map<String, Object> queryPageListBy(
            @RequestParam("domain") String dom, @RequestParam("valu") String value, int page, int limit) {
        if (dom.equals("1")) {
            dom = "title";
        }
        if (dom.equals("2")) {
            dom = "message";
        }
        List<Text> textList = textMapper.findTextby(dom, value, page, limit);
        int count = textService.count();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("msg", "成功");
        map.put("code", 0);
        map.put("count", count);
        map.put("data", textList);
        return map;
    }

    /**
     * 添加
     *
     * @param text
     * @return
     */
    @AutoLog(value = "text-添加")
    @ApiOperation(value = "text-添加", notes = "text-添加")
    @RequestMapping(value = "/add")
    public Result<?> add(@RequestParam("title") String title, @RequestParam("message") String message) {
        Text t = new Text();
        t.setId(0);
        t.setTitle(title);
        t.setMessage(message);
        t.setTime(new Date());
        textService.save(t);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param text
     * @return
     */
    @AutoLog(value = "text-编辑")
    @ApiOperation(value = "text-编辑", notes = "text-编辑")
    @RequestMapping(value = "/edit")
    public Result<?> edit(@RequestParam("id") int id, @RequestParam("title") String title, @RequestParam("message") String message) {
        Text t = new Text();
        t.setId(id);
        t.setTitle(title);
        t.setMessage(message);
        t.setTime(new Date());
        textService.updateById(t);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "text-通过id删除")
    @ApiOperation(value = "text-通过id删除", notes = "text-通过id删除")
    @PostMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        textService.removeById(id);
        return Result.ok("删除成功!");
    }


}
