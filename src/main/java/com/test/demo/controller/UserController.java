package com.test.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.test.demo.entity.User;
import com.test.demo.mapper.UserMapper;
import com.test.demo.service.IUserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description: text
 * @Author: moli
 * @Date: 2019-12-06
 * @Version: V1.0
 */
@Slf4j
@Api(tags = "text")
@RestController
@RequestMapping("user")
public class UserController extends JeecgController<User, IUserService> {
    @Autowired
    private IUserService userService;
    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/login")
    private Result<?> login(@RequestParam("username") String username, @RequestParam("password") String password, HttpSession session) {
        JSONObject jsonObject = new JSONObject();
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(User::getUsername, username)
                .eq(User::getPassword, password);
        List<User> userList = userMapper.selectList(queryWrapper);
        if (null == userList || userList.size() == 0) {
            return Result.error("用户名或密码错误");
        }
        jsonObject.put("username", username);
        jsonObject.put("code", 200);
        jsonObject.put("msg", "登录成功！");
        session.setAttribute("session_user", jsonObject);
        return Result.ok("登录成功");

    }


}
