package com.test.demo.controller;

import com.test.demo.entity.Text;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: moli
 * @Email: 974751082@qq.com
 * @qq: 974751082
 * @Date: 2019/12/6 21:29
 */
@Controller
public class ModelController {
    @RequestMapping("/")
    private String toTest() {
        return "test";
    }

    @RequestMapping("/login")
    private String tologin() {
        return "login";
    }

    @RequestMapping("/text/toTextEdit")
    private String toTextEdit(Text text, Model model) {
        model.addAttribute("text", text);
        return "editText";
    }

}
