package com.test.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.demo.entity.Text;

/**
 * @Description: text
 * @Author: moli
 * @Date:   2019-12-06
 * @Version: V1.0
 */
public interface ITextService extends IService<Text> {
}
