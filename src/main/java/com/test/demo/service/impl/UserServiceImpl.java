package com.test.demo.service.impl;

import com.test.demo.entity.User;
import com.test.demo.mapper.UserMapper;
import com.test.demo.service.IUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: text
 * @Author: moli
 * @Date:   2019-12-06
 * @Version: V1.0
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
