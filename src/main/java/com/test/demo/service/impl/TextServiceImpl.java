package com.test.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.demo.entity.Text;
import com.test.demo.mapper.TextMapper;
import com.test.demo.service.ITextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: text
 * @Author: moli
 * @Date: 2019-12-06
 * @Version: V1.0
 */
@Service("textService")
public class TextServiceImpl extends ServiceImpl<TextMapper, Text> implements ITextService {
    @Autowired
    private TextMapper textMapper;

}
