package com.test.demo.service;

import com.test.demo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: text
 * @Author: moli
 * @Date:   2019-12-06
 * @Version: V1.0
 */
public interface IUserService extends IService<User> {

}
